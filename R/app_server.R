#' The application server-side
#'
#' @param input,output,session Internal parameters for {shiny}.
#'     DO NOT REMOVE.
#' @import shiny
#' @noRd
app_server <- function(input, output, session) {
  # List the first level callModules here
  df <- mod_select_data_server("df")
  mod_spark_box_server("spaut", df, "Autorisations")
  mod_spark_box_server("spmec", df, "Mises en chantier")
  mod_map_server("map", df, "Mises en chantier")
  mod_table_server("table",df)
  mod_graphic_series_server("aut", df, "Autorisations")
  mod_graphic_series_server("mec", df, "Mises en chantier")
  mod_graphic_series_by_type_logement_server("aut_type_logement", df, "Autorisations")
  mod_graphic_series_by_type_logement_server("mec_type_logement", df, "Mises en chantier")
  mod_graphic_series_indice_server("aut_indice", df, "Autorisations")
  mod_graphic_series_indice_server("mec_indice", df, "Mises en chantier")
  mod_graphic_series_by_departement_server("aut_par_departement", df, "Autorisations")
  mod_graphic_series_by_departement_server("mec_par_departement", df, "Mises en chantier")
  shinyalert::shinyalert(
    title = "Bievenue",
    text = "Cette application vous permet de visualiser les statistiques de la construction neuve en France",
    closeOnEsc = TRUE,
    closeOnClickOutside = TRUE,
    html = FALSE,
    type = "",
    showConfirmButton = TRUE,
    showCancelButton = FALSE,
    confirmButtonText = "OK",
    confirmButtonCol = "#AEDEF4",
    timer = 0,
    imageUrl = "",
    animation = TRUE
  )
  
  output$title_ss_ter_panel = renderText({
    
    if(df()$reg == "France m\u00e9tropolitaine et DROM") {
      return("Par r\u00E9gions")
    }
    else {
      return("Par d\u00E9partements")
    }
    
  })
  output$title_ss_ter_panel_mec = renderText({
    
    if(df()$reg == "France m\u00e9tropolitaine et DROM") {
      return("Par r\u00E9gions")
    }
    else {
      return("Par d\u00E9partements")
    }
    
  })
  
}
