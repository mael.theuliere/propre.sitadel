#' série temporelle pour une région ou la France entière sur les autorisations ou les mises en chantier par type de logements
#'
#' @param data le df avec les séries
#' @param indic Autorisations ou Mises en chantier
#' @param zone la Zone sur laquelle afficher la série
#' @return un htmlWidget
#' @export
#'
#' @examples
#' library(dplyr)
#' library(COGiter)
#' data <- filter_data_input("Pays de la Loire")
#' make_graphic_series_by_type_logement(
#'   data = data$df,
#'   indic = "Mises en chantier",
#'   zone = data$reg
#' )
#' @importFrom apexcharter apex ax_stroke ax_labs ax_yaxis ax_tooltip format_num JS aes ax_chart
#' @importFrom glue glue
#' @importFrom stringr str_to_lower
make_graphic_series_by_type_logement <- function(data = NULL,
                                   indic = NULL,
                                   zone =NULL) {
  indic_lower <- str_to_lower(indic)
  type_zone <- "R\u00e9gions"
  if (zone == "France m\u00e9tropolitaine et DROM") {type_zone <- "France"}
  
  data %>%
    dplyr::filter(.data$indicateur == indic, 
                  .data$TypeLogement != "Logements",
                  .data$TypeZone == type_zone,
                  .data$Zone == zone) %>%
    apex(type = "area", mapping = aes(x = .data$date, y = .data$cumul12,fill=TypeLogement)) %>%
    ax_chart(stacked = TRUE) %>%
    ax_stroke(curve = "smooth") %>%
    ax_labs(
      title = glue("Evolution des {indic_lower}"),
      subtitle = glue("Source : Sitadel, estimations \u00e0 fin {lib}")
    ) %>%
    ax_yaxis(
      title = "",
      min = 0,
      labels = list(formatter = format_num(",.0f", locale = "fr-FR")),
      forceNiceScale = TRUE
    ) %>%
    ax_tooltip(
      x = list(formatter = JS(
        "function(date) {
            var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            year = d.getFullYear();
        
        if (month.length < 2) 
          month = '0' + month;

        return [year, month].join('-');
      }"
      ))
    )
}
