#' Graphique à facet sur les sous territoire du territoire sélectionné
#' Si le territoire est la France, affichage des graphiques de la régions, si c'est une région, affichage des départements de la région
#'
#' @param data le df avec les séries
#' @param zone la Zone sur laquelle afficher la série
#' @param indic Autorisations ou Mises en chantier
#' library(COGiter)
#' data <- filter_data_input("Pays de la Loire")
#' make_graphic_series_by_departement(
#'   data = data$df,
#'   indic = "Mises en chantier",
#'   zone = data$reg
#' )
#'
#' @return un graphique apexcharter.
#' @export
#' @importFrom apexcharter apex aes ax_stroke ax_labs ax_yaxis format_num ax_tooltip ax_facet_wrap vars
#' @importFrom dplyr filter mutate
#' @importFrom forcats fct_drop
#' @importFrom glue glue
#' @importFrom htmlwidgets JS
#' @importFrom stringr str_to_lower
make_graphic_series_by_departement <- function(data = NULL,
                                               zone = NULL,
                                               indic = NULL) {
  indic_lower <- str_to_lower(indic)
  if (zone == "France m\u00e9tropolitaine et DROM") {
    data <- data %>%
      dplyr::filter(TypeLogement == "Logements", TypeZone == "R\u00e9gions") %>%
      dplyr::select(Zone, indicateur,date,cumul12, cumul12_evolution)
  }
  else {
    data <- data %>%
      dplyr::filter(TypeLogement == "Logements", TypeZone == "D\u00e9partements") %>%
      dplyr::select(Zone, indicateur, date, cumul12, cumul12_evolution)
  }

  ax <- data %>%
    dplyr::filter(.data$indicateur==indic) %>%
    dplyr::mutate(Zone=forcats::fct_drop(.data$Zone)) %>%
    apexcharter::apex(type = "line",
                      mapping = apexcharter::aes(x = .data$date,
                                                 y = .data$cumul12)
                      ) %>%
    apexcharter::ax_facet_wrap(facets = apexcharter::vars(.data$Zone)
                               ) %>%
    apexcharter::ax_stroke(curve = "smooth") %>%
    apexcharter::ax_labs(
      title = glue::glue("Evolution des {indic_lower}"),
      subtitle = glue::glue("Source : Sitadel, estimations \u00e0 fin {lib}")
    ) %>%
    apexcharter::ax_yaxis(
      min = 0,
      labels = list(formatter = apexcharter::format_num(",.0f", locale = "fr-FR")),
      forceNiceScale = TRUE
    ) %>%
    apexcharter::ax_tooltip(
      x = list(formatter = htmlwidgets::JS(
        "function(date) {
            var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            year = d.getFullYear();
        if (month.length < 2)
          month = '0' + month;
        return [year, month].join('-');
      }"
      )),
      y = list(title = list(formatter = htmlwidgets::JS(glue::glue("function(title) {{return '{indic}';}}"))))
    )
return(ax)
  
  }
