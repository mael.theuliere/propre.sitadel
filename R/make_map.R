#' carte départementale
#'
#' @param data un dataframe
#'
#' @return un html widget
#' @export
#' @importFrom leaflet colorBin leaflet addProviderTiles providers addPolygons addCircles highlightOptions labelOptions
make_sitadel_map_dep <- function(data) {
  data_point <- data %>%
    dplyr::inner_join(dep_geo_centroid, .)
  data_poly <- data %>%
    dplyr::inner_join(dep_geo, .)
  pal <- colorBin(c("tomato", "light green"), domain = data$cumul12_evolution, bins = c(-Inf, 0, Inf))
  labels <- sprintf(
    "<strong>%s</strong><br/>%g Mises en chantier<br/>%+.1f%%  sur un an",
    data$Zone, data$cumul12, data$cumul12_evolution
  ) %>% lapply(htmltools::HTML)
  map <- leaflet(data_point) %>%
    addProviderTiles(providers$CartoDB.Positron) %>%
    addPolygons(
      data = data_poly,
      fillColor = "#ffffff",
      opacity = 1,
      color = "grey",
      dashArray = "3",
      fillOpacity = 0.7,
      weight = 1
    ) %>%
    addCircles(
      data = data_point,
      fillColor = ~ pal(cumul12_evolution),
      radius = ~ 4 * (cumul12),
      weight = 2,
      opacity = 1,
      color = "white",
      dashArray = "3",
      fillOpacity = 0.7,
      highlight = leaflet::highlightOptions(
        weight = 5,
        color = "#666",
        dashArray = "",
        fillOpacity = 0.7,
        bringToFront = TRUE
      ),
      label = labels,
      labelOptions = labelOptions(
        style = list("font-weight" = "normal", padding = "3px 8px"),
        textsize = "15px",
        direction = "auto"
      )
    )
  return(map)
}


#' carte départementale DROM
#'
#' @param data un dataframe
#'
#' @return un html widget
#' @export
#' @importFrom leaflet colorBin leaflet addProviderTiles providers addPolygons addCircles highlightOptions labelOptions
make_sitadel_map_drom <- function(data,zone) {
  data_point <- data %>%
    dplyr::inner_join(dep_geo_drom_centroid, .)
  data_poly <- data %>%
    dplyr::inner_join(departements_drom_geo, .)
  pal <- colorBin(c("tomato", "light green"), domain = data$cumul12_evolution, bins = c(-Inf, 0, Inf))
  labels <- sprintf(
    "<strong>%s</strong><br/>%g Mises en chantier<br/>%+.1f%%  sur un an",
    data$Zone, data$cumul12, data$cumul12_evolution
  ) %>% lapply(htmltools::HTML)
  map <- leaflet(data_point) %>%
    addProviderTiles(providers$CartoDB.Positron) %>%
    addPolygons(
      data = data_poly,
      fillColor = "#ffffff",
      opacity = 1,
      color = "grey",
      dashArray = "3",
      fillOpacity = 0.7,
      weight = 1
    ) %>%
    addCircles(
      data = data_point,
      fillColor = ~ pal(cumul12_evolution),
      radius = ~ 4 * (cumul12),
      weight = 2,
      opacity = 1,
      color = "white",
      dashArray = "3",
      fillOpacity = 0.7,
      highlight = leaflet::highlightOptions(
        weight = 5,
        color = "#666",
        dashArray = "",
        fillOpacity = 0.7,
        bringToFront = TRUE
      ),
      label = labels,
      labelOptions = labelOptions(
        style = list("font-weight" = "normal", padding = "3px 8px"),
        textsize = "15px",
        direction = "auto"
      )
    )
  return(map)
}
#' carte régionale
#'
#' @param data un dataframe
#'
#' @return un html widget
#' @export
#' @importFrom leaflet colorBin leaflet addProviderTiles providers addPolygons addCircles highlightOptions labelOptions
make_sitadel_map_reg <- function(data) {
  data_point <- data %>%
    dplyr::inner_join(reg_geo_centroid, .)
  data_poly <- data %>%
    dplyr::inner_join(reg_geo, .)
  pal <- colorBin(c("tomato", "light green"), domain = data$cumul12_evolution, bins = c(-Inf, 0, Inf))
  labels <- sprintf(
    "<strong>%s</strong><br/>%g Mises en chantier<br/>%+.1f%%  sur un an",
    data$Zone, data$cumul12, data$cumul12_evolution
  ) %>% lapply(htmltools::HTML)
  map <- leaflet(data_point) %>%
    addProviderTiles(providers$CartoDB.Positron) %>%
    addPolygons(
      data = data_poly,
      fillColor = "#ffffff",
      opacity = 1,
      color = "grey",
      dashArray = "3",
      fillOpacity = 0.7,
      weight = 1
    ) %>%
    addCircles(
      data = data_point,
      fillColor = ~ pal(cumul12_evolution),
      radius = ~ 2 * (cumul12),
      weight = 2,
      opacity = 1,
      color = "white",
      dashArray = "3",
      fillOpacity = 0.7,
      highlight = leaflet::highlightOptions(
        weight = 5,
        color = "#666",
        dashArray = "",
        fillOpacity = 0.7,
        bringToFront = TRUE
      ),
      label = labels,
      labelOptions = labelOptions(
        style = list("font-weight" = "normal", padding = "3px 8px"),
        textsize = "15px",
        direction = "auto"
      )
    )
  return(map)
}
#' carte régionale ou départementale
#'
#' @param data un dataframe
#' @param indic Autorisations ou Mises en chantier
#' @param zone la Zone sur laquelle afficher la série
#' @return un html widget
#' @export
make_sitadel_map <- function(data,zone,indic) {

  if (zone == "France m\u00e9tropolitaine et DROM") {
      data <- data %>%
        dplyr::filter(TypeLogement == "Logements",
                      indicateur == indic, 
                      TypeZone == "R\u00e9gions", 
                      date == max(date)) %>%
        dplyr::select(REG = CodeZone, Zone, cumul12, 
                      cumul12_evolution)
      
      return(make_sitadel_map_reg(data))
  } 
  if (COGiter::code_zone("Régions",zone) %in% c("01","02","03","04","06")) {
    data <- data %>%
      dplyr::filter(TypeLogement == "Logements", indicateur == indic,  TypeZone == "D\u00e9partements", date == max(date)) %>%
      dplyr::select(DEP = CodeZone,Zone, cumul12, cumul12_evolution)
    
    return(make_sitadel_map_drom(data)
    )
    
  }
  else {
      data <- data %>%
        dplyr::filter(TypeLogement == "Logements", 
                      indicateur == indic,  
                      TypeZone == "D\u00e9partements", 
                      date == max(date)) %>%
        dplyr::select(DEP = CodeZone,Zone, cumul12, cumul12_evolution)
      
      return(make_sitadel_map_dep(data)
             )
  }
}
