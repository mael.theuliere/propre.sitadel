#' table UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList 
#' @importFrom reactable reactableOutput
mod_table_ui <- function(id){
  ns <- NS(id)
  tagList(
    reactableOutput(ns("table"))
  )
}
    
#' table Server Function
#'
#' @noRd 
#' @importFrom reactable renderReactable

mod_table_server <- function(id,df){
  moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$table <- renderReactable({
        make_table(data = df()$df,
                   zone = df()$reg)
      })
    }
  )
}
    
