#' spark_box UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
#' @importFrom apexcharter sparkBoxOutput
mod_spark_box_ui <- function(id) {
  ns <- NS(id)
  tagList(
    sparkBoxOutput(ns("sparkbox"))
  )
}

#' spark_box Server Function
#'
#' @noRd
#' @importFrom apexcharter renderSparkBox
mod_spark_box_server <- function(id, df, indicateur) {
  moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$sparkbox <- renderSparkBox({
        sitadel_spark_box(data = df()$df, indic = indicateur,zone= df()$reg)
      })
    }
  )
}

## To be copied in the UI
# mod_spark_box_ui("spark_box_ui_1")

## To be copied in the server
# callModule(mod_spark_box_server, "spark_box_ui_1")
