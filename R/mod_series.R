#' Module UI pour le graphique en série temporelle de base
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
#' @importFrom apexcharter apexchartOutput
mod_graphic_series_ui <- function(id) {
  ns <- NS(id)
  tagList(
    apexchartOutput(ns("series"))
  )
}

#' Module server pour le graphique en série temporelle de base
#'
#' @noRd
#' @importFrom apexcharter renderApexchart
mod_graphic_series_server <- function(id, df, indicateur) {
  moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$series <- renderApexchart({
        make_graphic_series(data = df()$df, 
                  indic = indicateur,
                  zone=df()$reg)
      })
    }
  )
}

#' Module UI pour le graphique en série temporelle par type de logement
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
#' @importFrom apexcharter apexchartOutput
mod_graphic_series_by_type_logement_ui <- function(id) {
  ns <- NS(id)
  tagList(
    apexchartOutput(ns("series_par_type"))
  )
}
#' Module server pour le graphique en série temporelle par type de logement#'
#' @noRd
#' @importFrom apexcharter renderApexchart
mod_graphic_series_by_type_logement_server <- function(id, df, indicateur) {
  moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$series_par_type <- renderApexchart({
        make_graphic_series_by_type_logement(data = df()$df, 
                                     indic = indicateur,
                                     zone=df()$reg)
      })
    }
  )
}

#' Module UI pour le graphique en série temporelle en indice
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
#' @importFrom apexcharter apexchartOutput
mod_graphic_series_indice_ui <- function(id) {
  ns <- NS(id)
  tagList(
    apexchartOutput(ns("series_indice"))
  )
}
#' Module server pour le graphique en série temporelle en indice
#'
#' @noRd
#' @importFrom apexcharter renderApexchart
mod_graphic_series_indice_server <- function(id, df, indicateur) {
  moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$series_indice <- renderApexchart({
        make_graphic_series_indice(data = df()$df, 
                                             indic = indicateur,
                                             zone=df()$reg)
      })
    }
  )
}



#' Module UI pour le graphique en série temporelle par sous territoire
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
#' @importFrom apexcharter apexfacetOutput
mod_graphic_series_by_departement_ui <- function(id) {
  ns <- NS(id)
  tagList(
    apexfacetOutput(ns("series_par_dep"))
  )
}
#' Module server pour le graphique en série temporelle par sous territoire
#'
#' @noRd
#' @importFrom apexcharter renderApexfacet
mod_graphic_series_by_departement_server <- function(id, df, indicateur) {
  moduleServer(
    id,
    function(input, output, session) {
      ns <- session$ns
      output$series_par_dep <- renderApexfacet({
        make_graphic_series_by_departement(data = df()$df,
                                           zone = df()$reg,
                                           indic = indicateur)
      })
    }
  )
}